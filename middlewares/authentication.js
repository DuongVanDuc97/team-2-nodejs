require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require('../models').User;
const Employee = require('../models').Employee;
const Role = require('../models').Role;
const asyncHandler = require('../utils/asyncHandler');
const {promisify} = require('util');
const {
  UnauthorizedError,
  AuthFailureError,
  JsonWebTokenError,
} = require('../core/ApiError');

module.exports = asyncHandler(async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }

  if (!token) {
    return next(new UnauthorizedError());
  }
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_KEY);
  if (!decoded) {
    return next(new UnauthorizedError());
  }
  const {username} = decoded;
  const user = await User.findOne({
    where: {username},
    include: [
      {
        model: Employee,
        as: 'employee',
        include: [{model: Role, as: 'role'}],
      },
    ],
  });

  if (!user) {
    return next(new AuthFailureError('User not registered'));
  }

  // console.log(user.employee.role.code);
  // console.log(RoleCode);
  req.user = user.employee;
  next();
});
