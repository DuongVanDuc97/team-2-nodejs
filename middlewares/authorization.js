// const asyncHandler = require('../utils/asyncHandler');

const {UnauthorizedError} = require('../core/ApiError');

module.exports = function (...roles) {
  return (req, res, next) => {
    const roleCode = req.user.role.code;
    let authorized;
    authorized = roles.includes(roleCode);
    if (authorized) return next();

    next(new UnauthorizedError('Permission denied'));
  };
};
