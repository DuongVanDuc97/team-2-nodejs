const express = require('express');
const customerRouter = require('./routes/customer');
const employeeRouter = require('./routes/employee');
const userRouter = require('./routes/user');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const YAML = require('yaml');
const file = fs.readFileSync('./swagger.yaml', 'utf8');
const swaggerDocument = YAML.parse(file);
const {CustomError, NotFoundError, JsonWebTokenError} = require('./core/ApiError');

const app = express();
app.use(express.json());

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/customers', customerRouter);
app.use('/employees', employeeRouter);
app.use('/users', userRouter);

app.all('*', (req, res, next) => {
  next(new NotFoundError('Resources not found', 404));
});

app.use((err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  if (err instanceof CustomError) {
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  }
  if (err.name === 'JsonWebTokenError') {
    // JWT malformed error handling
    return res.status(401).json({
      status: 'fail',
      message: 'Invalid token',
    });
  }
  console.log(err);
  res.status(500).json({
    status: err.status,
    message: 'Internal Server Error!',
  });
});

module.exports = app;
