const RoleCode = {
  PRESIDENT: 'PRESIDENT',
  MANAGER: 'MANAGER',
  LEADER: 'LEADER',
  STAFF: 'STAFF',
};

module.exports = {RoleCode};
