const Employee = require('../models').Employee;
const asyncHandler = require('../utils/asyncHandler');

class EmployeeController {
  createEmployee = asyncHandler(async (req, res, next) => {
    const {
      firstName,
      lastName,
      extension,
      email,
      officeCode,
      reportsTo,
      jobTitle,
      roleId,
    } = req.body;

    const employee = Employee.build({
      firstName,
      lastName,
      extension,
      email,
      officeCode,
      reportsTo,
      jobTitle,
      roleId,
    });

    const savedEmployee = await employee.save();

    return res.status(201).json({
      status: 'Success',
      data: savedEmployee,
    });
  });

  getListEmployees = asyncHandler(async (req, res, next) => {
    const employees = await Employee.findAll();

    return res.status(200).json({
      status: 'Success',
      data: employees,
    });
  });

  getEmployeeById = asyncHandler(async (req, res, next) => {
    const {id} = req.params;
    const employee = await Employee.findByPk(id);
    if (!employee) {
      res.status(400).json({
        message: 'fail not found',
      });
    }

    return res.status(200).json({
      status: 'Success',
      data: employee,
    });
  });

  updateEmployeeById = asyncHandler(async (req, res, next) => {
    const {id} = req.params;
    const employee = await Employee.findByPk(id);
    if (!employee) {
      res.status(400).json({
        message: 'fail not found',
      });
    }

    const allowedUpdates = [
      'firstName',
      'lastName',
      'extension',
      'email',
      'officeCode',
      'reportsTo',
      'jobTitle',
      'roleId',
    ];
    for (const key in req.body) {
      if (allowedUpdates.includes(key)) {
        employee[key] = req.body[key];
      }
    }
    await employee.save();
    return res.status(200).json({
      status: 'Success',
      data: employee,
    });
  });

  deleteEmployee = asyncHandler(async (req, res, next) => {
    const {id} = req.params;

    const employee = await Employee.findByPk(id);
    if (!employee) {
      res.status(400).json({
        message: 'fail not found',
      });
    }

    console.log(employee);

    await Employee.destroy({
      where: {
        employeeNumber: id,
      },
    });
    return res.status(200).json({
      status: 'Success',
      data: null,
    });
  });
}
module.exports = new EmployeeController();
