const Customer = require('../models').Customer;
const {Op, Sequelize} = require('sequelize');

class CustomerController {
  getListCustomers = async (req, res) => {
    const customers = await Customer.findAll();

    return res.status(200).json({
      status: 'Success',
      result: customers.length,
      data: customers,
    });
  };

  createCustomer = async (req, res, next) => {
    const {
      customerName,
      contractLastName,
      contractFirstName,
      phone,
      addressLine1,
      addressLine2,
      city,
      state,
      postalCode,
      country,
      salesRepEmployeeNumber,
      creditLimit,
    } = req.body;

    const customer = await Customer.create({
      customerName,
      contractLastName,
      contractFirstName,
      phone,
      addressLine1,
      addressLine2,
      city,
      state,
      postalCode,
      country,
      salesRepEmployeeNumber,
      creditLimit,
    });

    return res.status(201).json({
      status: 'Success',
      data: customer,
    });
  };
}

module.exports = new CustomerController();
