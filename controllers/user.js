const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models').User;
const asyncHandler = require('../utils/asyncHandler');
const {BadRequestError, InternalServerError} = require('../core/ApiError');
const SALT_ROUNDS = 10;
require('dotenv').config();

exports.signin = asyncHandler(async (req, res, next) => {
  const {username, password} = req.body;

  const user = await User.findOne({where: {username}});

  if (!user || !(await bcrypt.compare(password, user.password))) {
    return next(new BadRequestError('Invalid credentials'));
  }

  const token = jwt.sign({username}, process.env.JWT_KEY);

  res.status(200).json({
    status: 'Success',
    token,
  });
});

exports.signup = asyncHandler(async (req, res, next) => {
  const {username, password, employeeNumber} = req.body;

  const existingUser = await User.findOne({where: {username}});
  if (existingUser) {
    return next(new BadRequestError('User already registered'));
  }

  const hashedPassword = await bcrypt.hash(password, SALT_ROUNDS);

  const newUser = await User.create({
    username,
    password: hashedPassword,
    employeeNumber,
  });

  if (!newUser) {
    return next(
      new InternalServerError('An error occurred. Please try again later.'),
    );
  }

  const token = jwt.sign({username}, process.env.JWT_KEY);

  return res.status(201).json({
    status: 'Success',
    data: newUser,
    token,
  });
});
