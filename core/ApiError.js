class CustomError extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
  }
}

class BadRequestError extends CustomError {
  constructor(message = 'Bad Request') {
    super(message, 400);
  }
}

class AuthFailureError extends CustomError {
  constructor(message = 'Invalid Credentials') {
    super(message, 401);
  }
}

class InternalServerError extends CustomError {
  constructor(message = 'Internal Server Error') {
    super(message, 500);
  }
}
class UnauthorizedError extends CustomError {
  constructor(message = 'Unauthorized') {
    super(message, 401);
  }
}

class NotFoundError extends CustomError {
  constructor(message = 'Not Found') {
    super(message, 404);
  }
}

class JsonWebTokenError extends CustomError {
  constructor(message = 'Invalid token') {
    super(message, 401);
  }
}

module.exports = {
  CustomError,
  BadRequestError,
  InternalServerError,
  UnauthorizedError,
  NotFoundError,
  AuthFailureError,
  JsonWebTokenError,
};
