const app = require('./index');
const Logger = require('./core/Logger');
require('dotenv').config();

const PORT = process.env.PORT || 3000;

app
  .listen(PORT, () => {
    Logger.info(`server running on port : ${PORT}`);
    console.log(`Server is running on port ${PORT}`);
  })
  .on('error', e => Logger.error(e));
