const express = require('express');
const customerController = require('../controllers/customer');
const authentication = require('../middlewares/authentication');
const authorization = require('../middlewares/authorization');
const validator = require('../utils/validator');
const schema = require('../models/schemas/customer');
const {RoleCode} = require('../constant/roles');

const router = express.Router();

router.use(authentication);

router.get('/', customerController.getListCustomers);
router.post('/', customerController.createCustomer);

module.exports = router;
