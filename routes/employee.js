const express = require('express');
const employeeController = require('../controllers/employee');
const authentication = require('../middlewares/authentication');
const authorization = require('../middlewares/authorization');
const validator = require('../utils/validator');
const schema = require('../models/schemas/employee');
const {RoleCode} = require('../constant/roles');

const router = express.Router();

router.use(authentication);

router
  .route('/')
  .get(
    authorization(RoleCode.PRESIDENT, RoleCode.MANAGER, RoleCode.LEADER),
    employeeController.getListEmployees,
  )
  .post(
    authorization(RoleCode.PRESIDENT, RoleCode.MANAGER),
    validator(schema.createEmployee),
    employeeController.createEmployee,
  );

router
  .route('/:id')
  .get(
    authorization(RoleCode.PRESIDENT, RoleCode.MANAGER, RoleCode.LEADER),
    employeeController.getEmployeeById,
  )
  .put(
    authorization(RoleCode.PRESIDENT, RoleCode.MANAGER),
    validator(schema.updateEmployee),
    employeeController.updateEmployeeById,
  )
  .delete(authorization(RoleCode.PRESIDENT), employeeController.deleteEmployee);

module.exports = router;
