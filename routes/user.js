const express = require('express');
const userController = require('../controllers/user');
const router = express.Router();
const validator = require('../utils/validator');
const schema = require('../models/schemas/user');

router.post('/signup', validator(schema.signup), userController.signup);
router.post('/signin', validator(schema.credential), userController.signin);

module.exports = router;
