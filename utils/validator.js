const {BadRequestError} = require('../core/ApiError');
const Logger = require('../core/Logger');

ValidationSource = {
  BODY: 'body',
  HEADER: 'headers',
  QUERY: 'query',
  PARAM: 'params',
};

module.exports =
  (schema, source = ValidationSource.BODY) =>
  (req, res, next) => {
    try {
      const {error} = schema.validate(req[source]);

      if (!error) return next();

      const {details} = error;

      const message = details
        .map(i => i.message.replace(/['"]+/g, ''))
        .join(',');
      Logger.error(message);

      next(new BadRequestError(message));
    } catch (error) {
      next(error);
    }
  };
