'use strict';
const Employee = require('../models').Employee;
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Employees',
      [
        {
          employeeNumber: 1,
          firstName: 'John',
          lastName: 'Doe',
          extension: '1234',
          email: 'john@example.com',
          officeCode: 2,
          reportsTo: null,
          jobTitle: 'President',
          roleId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 2,
          firstName: 'Jane',
          lastName: 'Smith',
          extension: '5678',
          email: 'jane@example.com',
          officeCode: 1,
          reportsTo: null,
          jobTitle: 'Manager',
          roleId: 2,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 3,
          firstName: 'David',
          lastName: 'Brown',
          extension: '4321',
          email: 'david@example.com',
          officeCode: 2,
          reportsTo: null,
          jobTitle: 'Leader',
          roleId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 4,
          firstName: 'Sarah',
          lastName: 'Johnson',
          extension: '8765',
          email: 'sarah@example.com',
          officeCode: 2,
          reportsTo: null,
          jobTitle: 'Leader',
          roleId: 3,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 5,
          firstName: 'Michael',
          lastName: 'Williams',
          extension: '1357',
          email: 'michael@example.com',
          officeCode: 1,
          reportsTo: null,
          jobTitle: 'Staff',
          roleId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 6,
          firstName: 'Emily',
          lastName: 'Jones',
          extension: '2468',
          email: 'emily@example.com',
          officeCode: 1,
          reportsTo: null,
          jobTitle: 'Staff',
          roleId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 7,
          firstName: 'William',
          lastName: 'Davis',
          extension: '3690',
          email: 'william@example.com',
          officeCode: 3,
          reportsTo: null,
          jobTitle: 'Staff',
          roleId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 8,
          firstName: 'Olivia',
          lastName: 'Taylor',
          extension: '2580',
          email: 'olivia@example.com',
          officeCode: 3,
          reportsTo: null,
          jobTitle: 'Staff',
          roleId: 4,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          employeeNumber: 9,
          firstName: 'John',
          lastName: 'Doe',
          extension: '1234',
          email: 'john@example.com',
          officeCode: 3,
          reportsTo: null,
          jobTitle: 'President',
          roleId: 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );

    const extistingEmployees = await Employee.count();

    await queryInterface.sequelize.query(
      `ALTER SEQUENCE "Employees_employeeNumber_seq" RESTART WITH ${
        extistingEmployees + 1
      }`,
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Employees', null, {});
  },
};
