'use strict';
const Office = require('../models').Office;

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Offices',
      [
        {
          officeCode: 1,
          city: 'New York',
          phone: '+1 212-555-1234',
          addressLine1: '123 Main St',
          addressLine2: 'Apt 4B',
          state: 'NY',
          country: 'USA',
          postalCode: '10001',
          territory: 'North America',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          officeCode: 2,
          city: 'Los Angeles',
          phone: '+1 310-555-5678',
          addressLine1: '456 Sunset Blvd',
          addressLine2: 'Suite 200',
          state: 'CA',
          country: 'USA',
          postalCode: '90028',
          territory: 'North America',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          officeCode: 3,
          city: 'Wibu sav3 w0lrd',
          phone: '+1 310-555-9999',
          addressLine1: '444 Sunset Blvd',
          addressLine2: 'Suite 330',
          state: 'HN',
          country: 'VN',
          postalCode: '1200',
          territory: 'North',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );

    const extistingOffices = await Office.count();
    await queryInterface.sequelize.query(
      `ALTER SEQUENCE "Offices_officeCode_seq" RESTART WITH ${
        extistingOffices + 1
      }`,
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Offices', null, {});
  },
};
