const Joi = require('joi');

module.exports = {
  credential: Joi.object().keys({
    username: Joi.string().min(3).max(20).required(),
    password: Joi.string().min(6).max(100).required(),
  }),

  signup: Joi.object().keys({
    username: Joi.string().min(3).max(20).required(),
    password: Joi.string().min(6).max(100).required(),
    employeeNumber: Joi.number().positive().required(),
  }),
};
