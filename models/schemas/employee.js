const Joi = require('joi');

module.exports = {
  createEmployee: Joi.object().keys({
    employeeNumber: Joi.number().positive(),
    lastName: Joi.string().min(3).max(50).required(),
    firstName: Joi.string().min(3).max(50).required(),
    extension: Joi.string().max(50).required(),
    email: Joi.string().email().min(10).max(100).required(),
    officeCode: Joi.number().max(10).required(),
    reportsTo: Joi.number().positive().allow(null),
    jobTitle: Joi.string().valid('President', 'Manager', 'Leader').required(),
    roleId: Joi.number().required(),
  }),
  updateEmployee: Joi.object().keys({
    employeeNumber: Joi.number().positive(),
    lastName: Joi.string().min(3).max(50),
    firstName: Joi.string().min(3).max(50),
    extension: Joi.string().max(50),
    email: Joi.string().email().min(10).max(100),
    officeCode: Joi.number().max(10),
    reportsTo: Joi.number().positive().allow(null),
    jobTitle: Joi.string().valid('President', 'Manager', 'Leader'),
    roleId: Joi.number(),
  }),
};
