'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.addConstraint('Users', {
      fields: ['employeeNumber'],
      type: 'FOREIGN KEY',
      references: {
        name: 'employeeNumber-as-fk-users',
        table: 'Employees',
        field: 'employeeNumber',
      },
    });
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.removeConstraint('Users', 'employeeNumber-as-fk-users');
  },
};
