'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn('Employees', 'roleId', {
      type: Sequelize.INTEGER,
      allowNull: false,
      references: {
        model: 'Roles',
        key: 'id',
      },
      onUpdate: 'CASCADE',
    });

    await queryInterface.addConstraint('Employees', {
      fields: ['roleId'],
      type: 'FOREIGN KEY',
      references: {
        name: 'fk_employee_role',
        table: 'Roles',
        field: 'id',
      },
      onUpdate: 'CASCADE',
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint('Employees', 'fk_employee_role');
    await queryInterface.removeColumn('Employees', 'roleId');
  },
};
