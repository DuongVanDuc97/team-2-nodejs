'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.addConstraint('Employees', {
      fields: ['officeCode'],
      type: 'FOREIGN KEY',
      references: {
        name: 'officeCode-as-fk-employees',
        table: 'Offices',
        field: 'officeCode',
      },
    });
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.removeConstraint(
      'Employees',
      'officeCode-as-fk-employees',
    );
  },
};
