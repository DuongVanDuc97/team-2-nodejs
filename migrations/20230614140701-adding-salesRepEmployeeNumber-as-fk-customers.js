'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.addConstraint('Customers', {
      fields: ['salesRepEmployeeNumber'],
      type: 'FOREIGN KEY',
      references: {
        name: 'salesRepEmployeeNumber-as-fk-customers',
        table: 'Employees',
        field: 'employeeNumber',
      },
    });
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.removeConstraint('Customers', 'salesRepEmployeeNumber-as-fk-customers');
  },
};
